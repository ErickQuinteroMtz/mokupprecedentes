import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'mockupSentencias';
  ngOnInit(): void {
    const stepButtons = document.querySelectorAll('.step-button');
    const progress:HTMLElement = document.getElementById('progress') as HTMLElement;
    

    Array.from(stepButtons).forEach((button,index) => {
    button.addEventListener('click', () => {
        progress.setAttribute('value', String(index * 100 /(stepButtons.length - 1)) );//there are 3 buttons. 2 spaces.bena 
        stepButtons.forEach((item, secindex)=>{
            if(index > secindex){
                item.classList.add('done');
            }
            if(index < secindex){
                item.classList.remove('done');
            }
        })
    })
  })    
  }
  
}
