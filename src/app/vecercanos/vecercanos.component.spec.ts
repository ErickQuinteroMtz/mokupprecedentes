import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VecercanosComponent } from './vecercanos.component';

describe('VecercanosComponent', () => {
  let component: VecercanosComponent;
  let fixture: ComponentFixture<VecercanosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VecercanosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VecercanosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
