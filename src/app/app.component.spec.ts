import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'mockupSentencias'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('mockupSentencias');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.content span')?.textContent).toContain('mockupSentencias app is running!');
  });


  const stepButtons = document.querySelectorAll('.step-button');
  const progress = document.querySelector('progress');

  Array.from(stepButtons).forEach((button,index) => {
      button.addEventListener('click', () => {
          progress.setAttribute('value', index * 100 /(stepButtons.length - 1));//there are 3 buttons. 2 spaces.

          stepButtons.forEach((item, secindex)=>{
              if(index > secindex){
                  item.classList.add('done');
              }
              if(index < secindex){
                  item.classList.remove('done');
              }
          })
      })
  });

});
