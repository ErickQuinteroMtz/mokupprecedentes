import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SentenciaComponent } from './sentencia/sentencia.component';
import { LimpiezaComponent } from './limpieza/limpieza.component';
import { VectorComponent } from './vector/vector.component';
import { BdComponent } from './bd/bd.component';
import { ClasificacionComponent } from './clasificacion/clasificacion.component';
import { PrecedentesComponent } from './precedentes/precedentes.component';
import { VecercanosComponent } from './vecercanos/vecercanos.component';

@NgModule({
  declarations: [
    AppComponent,
    SentenciaComponent,
    LimpiezaComponent,
    VectorComponent,
    BdComponent,
    ClasificacionComponent,
    PrecedentesComponent,
    VecercanosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
