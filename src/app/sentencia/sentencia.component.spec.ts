import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SentenciaComponent } from './sentencia.component';

describe('SentenciaComponent', () => {
  let component: SentenciaComponent;
  let fixture: ComponentFixture<SentenciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SentenciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SentenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
