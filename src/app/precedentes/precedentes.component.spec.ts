import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrecedentesComponent } from './precedentes.component';

describe('PrecedentesComponent', () => {
  let component: PrecedentesComponent;
  let fixture: ComponentFixture<PrecedentesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrecedentesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrecedentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
